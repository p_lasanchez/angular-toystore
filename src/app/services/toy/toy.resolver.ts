/**
 * ToysResolver
 *
 * - set Toys au resolve
 */
import { Injectable } from "@angular/core";
import { Resolve } from "@angular/router";
import { Subscription } from "rxjs";
import { ToyStore } from "src/app/store/toy/toy.store";

@Injectable({ providedIn: "root" })
export class ToyResolver implements Resolve<Subscription> {
  constructor(private toyStore: ToyStore) {}

  resolve(): Subscription {
    return this.toyStore.setToyList();
  }
}
