/*
 * Testing ToyService
 */

import { HttpClient } from "@angular/common/http";
import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { ToyService } from "./toy.service";

describe("ToysService", () => {
  let service: ToyService;

  const spyHttp = jasmine.createSpyObj("spyHttp", [
    "get",
  ]) as jasmine.SpyObj<HttpClient>;
  spyHttp.get.and.returnValue(of([]));

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToyService, { provide: HttpClient, useValue: spyHttp }],
    });

    service = TestBed.inject(ToyService);
  });

  it("should get toys", (done) => {
    service.getToys().subscribe((res) => {
      expect(res).toEqual([]);
      done();
    });
  });
});
