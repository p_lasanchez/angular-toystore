/**
 * ToysService
 *
 * - getToys
 *    . http://localhost:9000/toys
 */
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Toy } from "../../models/toy";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class ToyService {
  constructor(private http: HttpClient) {}

  public getToys = (): Observable<Toy[]> =>
    this.http.get<Toy[]>("http://localhost:9000/toys");
}
