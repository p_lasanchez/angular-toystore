/*
 * Testing ToysResolver
 */

import { TestBed } from "@angular/core/testing";
import { ToyResolver } from "./toy.resolver";
import { ToyStore } from "../../store/toy/toy.store";

describe("ToysResolver", () => {
  let resolver: ToyResolver;
  const spyToyStore: ToyStore = jasmine.createSpyObj("spyToyStore", [
    "setToyList",
  ]);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToyResolver, { provide: ToyStore, useValue: spyToyStore }],
    });

    resolver = TestBed.inject(ToyResolver);
  });

  it("shoudl resolve toys", () => {
    resolver.resolve();
    expect(spyToyStore.setToyList).toHaveBeenCalled();
  });
});
