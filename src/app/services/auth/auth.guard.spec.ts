/*
 * Testing AuthGuard
 */

import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { of } from "rxjs";

import { AuthGuard } from "./auth.guard";
import { AuthStore } from "src/app/store/auth/auth.store";
import { AuthService } from "src/app/services/auth/auth.service";

describe("AuthGuard", () => {
  let guard: AuthGuard;
  let store: AuthStore;
  const mockUser = { login: "hello", pass: "toto" };

  const spyService = jasmine.createSpyObj("service", [
    "getUser",
  ]) as jasmine.SpyObj<AuthService>;
  spyService.getUser.and.returnValue(of([mockUser]));

  const spyRouter = jasmine.createSpyObj("spyRouter", [
    "navigateByUrl",
  ]) as jasmine.SpyObj<Router>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        AuthStore,
        { provide: AuthService, useValue: spyService },
        { provide: Router, useValue: spyRouter },
      ],
    });

    guard = TestBed.inject(AuthGuard);
    store = TestBed.inject(AuthStore);
  });

  it("should not be connected", (done) => {
    guard.canActivate().subscribe((result) => {
      expect(result).toBeFalsy();
      done();
    });
  });

  it("should be connected", (done) => {
    store.setConnected(mockUser);

    expect(spyService.getUser).toHaveBeenCalledWith(mockUser);

    guard.canActivate().subscribe((result) => {
      expect(result).toBe(true);
      expect(spyRouter.navigateByUrl).toHaveBeenCalledWith("/basket");
      done();
    });
  });
});
