/*
 * Testing AuthService
 */

import { TestBed } from "@angular/core/testing";
import { AuthService } from "./auth.service";
import { HttpClient } from "@angular/common/http";
import { of } from "rxjs";

describe("AuthService", () => {
  let service: AuthService;
  const mockUser = { login: "toto", pass: "titi" };

  const spyHttp = jasmine.createSpyObj("http", [
    "get",
  ]) as jasmine.SpyObj<HttpClient>;
  spyHttp.get.and.returnValue(of([mockUser]));

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService, { provide: HttpClient, useValue: spyHttp }],
    });

    service = TestBed.inject(AuthService);
  });

  it("should get auth", () => {
    service.getUser(mockUser).subscribe((res) => {
      expect(res).toEqual([mockUser]);
      expect(spyHttp.get).toHaveBeenCalledWith(
        "http://localhost:9000/authentication?login=toto&pass=titi"
      );
    });
  });
});
