/**
 * AuthGuard
 *
 * - activate si connected
 */
import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { Observable } from "rxjs";

import { AuthStore } from "src/app/store/auth/auth.store";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {
  constructor(private authStore: AuthStore) {}

  canActivate(): Observable<boolean> {
    return this.authStore.getConnected();
  }
}
