/**
 * AuthService
 *
 * - getUser (credentials)
 *    . http://localhost:9000/authentication?login=xxx&pass=xxx
 */
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../../models/user";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class AuthService {
  constructor(private http: HttpClient) {}

  public getUser = (credentials: User): Observable<User[]> =>
    this.http.get<User[]>(
      `http://localhost:9000/authentication?login=${credentials.login}&pass=${credentials.pass}`
    );
}
