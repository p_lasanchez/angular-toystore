/**
 * User
 *
 * - login string
 * - pass string
 */
export interface User {
  login: string;
  pass: string;
}
