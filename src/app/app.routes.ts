/**
 * app Routes
 * ----------
 *
 * /toys => ToysComponent
 *  - preload toy data with ToyResolver
 *
 * /basket => BasketComponent
 *  - inaccessible si non connecté / AuthGuard
 *
 * /autre => redirection vers ToyListComponent
 */
import { Routes } from "@angular/router";
import { BasketComponent } from "./features/basket/basket.component";
import { ToyListComponent } from "./features/toylist/toylist.component";
import { ToyResolver } from "./services/toy/toy.resolver";

import { AuthGuard } from "./services/auth/auth.guard";

export const appRoutes: Routes = [
  {
    path: "toys",
    component: ToyListComponent,
    resolve: {
      toys: ToyResolver,
    },
  },
  {
    path: "basket",
    component: BasketComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "",
    redirectTo: "/toys",
    pathMatch: "full",
  },
  {
    path: "**",
    redirectTo: "/toys",
    pathMatch: "full",
  },
];
