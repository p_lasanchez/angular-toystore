/**
 * definir interfaces
 * - AuthState
 *   isConnected
 *   isOpened
 *   errror
 *
 * - ToysState
 *   toyList
 *   error
 */
import { Toy } from "../models/toy";

export interface AuthState {
  isConnected: boolean;
  isOpened: boolean;
  error: string;
}

export interface ToyState {
  toyList: Toy[];
  error: string;
}
