import { TestBed } from "@angular/core/testing";
import { of } from "rxjs";
import { ToyService } from "src/app/services/toy/toy.service";
import { ToyStore } from "./toy.store";

describe("ToyListStore", () => {
  let store: ToyStore;

  const mockToy1 = {
    title: "jouet",
    icon: "image",
    price: 10,
  };
  const mockToy2 = {
    title: "voiture",
    icon: "autre",
    price: 20,
  };

  const spyService = jasmine.createSpyObj("spyService", [
    "getToys",
  ]) as jasmine.SpyObj<ToyService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToyStore, { provide: ToyService, useValue: spyService }],
    });

    store = TestBed.inject(ToyStore);
  });

  it("Should have no toy", (done) => {
    store.getToyList().subscribe((res) => {
      expect(res).toEqual([]);
      done();
    });
  });

  it("Should set a toylist", (done) => {
    spyService.getToys.and.returnValue(of([mockToy1, mockToy2]));
    store.setToyList();

    store.getToyList().subscribe((res) => {
      expect(res).toEqual([mockToy1, mockToy2]);
      done();
    });
  });

  it("Should select a toy", (done) => {
    spyService.getToys.and.returnValue(of([mockToy1, mockToy2]));
    store.setToyList();
    store.setSelectedToy(mockToy2);

    store.getSelectedToys().subscribe((res) => {
      expect(res).toEqual([mockToy2]);
      done();
    });
  });

  it("Should count selected toys", (done) => {
    spyService.getToys.and.returnValue(
      of([
        { ...mockToy1, selected: true },
        { ...mockToy2, selected: true },
      ])
    );
    store.setToyList();

    store.getToyListCount().subscribe((res) => {
      expect(res).toEqual(2);
      done();
    });
  });

  it("Should get total price", (done) => {
    spyService.getToys.and.returnValue(
      of([
        { ...mockToy1, selected: true },
        { ...mockToy2, selected: true },
      ])
    );
    store.setToyList();

    store.getTotalPrice().subscribe((res) => {
      expect(res).toBe(30);
      done();
    });
  });
});
