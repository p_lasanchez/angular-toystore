/**
 * ToyStore
 *  - model : ToyState
 *
 * Actions
 *  - setToyList
 *  - setSelectedToy
 *
 *  - getError
 *  - getToyList
 *  - getSelectedToys
 *  - getToyListCount
 *  - getTotalPrice
 */
import { Injectable } from "@angular/core";
import { ToyService } from "../../services/toy/toy.service";
import { Subscription, BehaviorSubject, Observable } from "rxjs";
import { Toy } from "src/app/models/toy";
import { ToyState } from "..";
import { map } from "rxjs/operators";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable({ providedIn: "root" })
export class ToyStore {
  private state$: BehaviorSubject<ToyState>;

  constructor(private toyService: ToyService) {
    this.state$ = new BehaviorSubject<ToyState>({ toyList: [], error: "" });
  }

  setToyList = (): Subscription => {
    if (this.state$.getValue().toyList.length) {
      return;
    }
    return this.toyService.getToys().subscribe({
      next: (toyList) => {
        this.state$.next({ toyList, error: "" });
      },
      error: (error: HttpErrorResponse) => {
        this.state$.next({ toyList: [], error: error.message });
        console.log(error);
      },
    });
  };

  setSelectedToy = (toy: Toy): void => {
    const toyList = this.state$.getValue().toyList.map((item) => {
      item.selected = item.title === toy.title ? !item.selected : item.selected;
      return item;
    });
    this.state$.next({ toyList, error: "" });
  };

  getError = (): Observable<string> =>
    this.state$.pipe(map((state) => state.error));

  getToyList = (): Observable<Toy[]> =>
    this.state$.pipe(map((state) => state.toyList));

  getSelectedToys = (): Observable<Toy[]> =>
    this.getToyList().pipe(
      map((toyList) => toyList.filter((toy) => toy.selected))
    );

  getToyListCount = (): Observable<number> =>
    this.getSelectedToys().pipe(map((item) => item.length));

  getTotalPrice = (): Observable<number> =>
    this.getSelectedToys().pipe(
      map((selected) => selected.reduce((acc, item) => acc + item.price, 0))
    );
}
