/**
 * AuthStore
 * - model : AuthState
 *
 * Actions
 *  - setOpen
 *  - setClose
 *  - setConnected
 *
 *  - getConnected
 *  - getOpened
 *  - getError
 */
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { AuthService } from "../../services/auth/auth.service";
import { User } from "../../models/user";
import { Subscription, Observable, BehaviorSubject } from "rxjs";
import { AuthState } from "..";
import { map } from "rxjs/operators";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable({ providedIn: "root" })
export class AuthStore {
  private auth$: BehaviorSubject<AuthState>;

  constructor(private router: Router, private authService: AuthService) {
    this.auth$ = new BehaviorSubject<AuthState>({
      isConnected: false,
      isOpened: false,
      error: "",
    });
  }

  setOpen = (): void => {
    const previousAuth = this.auth$.getValue();
    this.auth$.next({ ...previousAuth, isOpened: true });
  };

  setClose = (): void => {
    const previousAuth = this.auth$.getValue();
    this.auth$.next({ ...previousAuth, isOpened: false });
  };

  setConnected = (credentials: User): Subscription => {
    const previousAuth = this.auth$.getValue();

    return this.authService.getUser(credentials).subscribe({
      next: (users: User[]) => {
        if (!users.length) {
          this.auth$.next({ ...previousAuth, error: "User inconnu" });
          return;
        }

        this.auth$.next({ ...previousAuth, isConnected: true });
        this.setClose();
        this.router.navigateByUrl("/basket");
      },
      error: (error: HttpErrorResponse) => {
        console.log(error);
        this.auth$.next({ ...previousAuth, error: error.message });
      },
    });
  };

  getConnected = (): Observable<boolean> =>
    this.auth$.pipe(map((auth) => auth.isConnected));

  getOpened = (): Observable<boolean> =>
    this.auth$.pipe(map((auth) => auth.isOpened));

  getError = (): Observable<string> =>
    this.auth$.pipe(map((state) => state.error));
}
