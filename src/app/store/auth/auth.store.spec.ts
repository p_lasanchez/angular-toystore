import { TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";

import { AuthStore } from "./auth.store";
import { AuthService } from "../../services/auth/auth.service";
import { of } from "rxjs";

describe("AuthStore", () => {
  let store: AuthStore;
  const spyService = jasmine.createSpyObj("spyService", [
    "getUser",
  ]) as jasmine.SpyObj<AuthService>;
  const spyRouter = jasmine.createSpyObj("spyRouter", [
    "navigateByUrl",
  ]) as jasmine.SpyObj<Router>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AuthStore,
        { provide: AuthService, useValue: spyService },
        { provide: Router, useValue: spyRouter },
      ],
    });

    store = TestBed.inject(AuthStore);
  });

  it("Should initialize", (done) => {
    store.getConnected().subscribe((res) => {
      expect(res).toBe(false);
    });

    store.getOpened().subscribe((res) => {
      expect(res).toBe(false);
      done();
    });
  });

  it("Should open", (done) => {
    store.setOpen();
    store.getOpened().subscribe((res) => {
      expect(res).toBe(true);
      done();
    });
  });

  it("Should close", (done) => {
    store.setOpen();
    store.setClose();
    store.getOpened().subscribe((res) => {
      expect(res).toBe(false);
      done();
    });
  });

  it("Should connect", (done) => {
    const mockUser = { login: "toto", pass: "titi" };
    spyService.getUser.and.returnValue(of([mockUser]));

    store.setOpen();

    store.setConnected(mockUser);
    expect(spyRouter.navigateByUrl).toHaveBeenCalledWith("/basket");

    store.getConnected().subscribe((res) => {
      expect(res).toBe(true);
    });

    store.getOpened().subscribe((res) => {
      expect(res).toBe(false);
      done();
    });
  });

  it("Should do nothing if no user", (done) => {
    const mockUser = { login: "toto", pass: "titi" };
    spyService.getUser.and.returnValue(of([]));

    store.setOpen();
    store.setConnected(mockUser);

    store.getOpened().subscribe((res) => {
      expect(res).toBe(true);
      done();
    });
  });
});
