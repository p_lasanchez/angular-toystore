/**
 * AppModule
 * - AppComponent
 *
 * - BrowserModule
 * - FeaturesModule
 * - RouterModule
 * - HttpClientModule
 *
 * bootstrap AppComponent
 */
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";

import { FeaturesModule } from "./features/features.module";

import { appRoutes } from "./app.routes";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FeaturesModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
