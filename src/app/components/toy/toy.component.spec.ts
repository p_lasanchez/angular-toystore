/*
 * Testing ToyComponent
 */
import { Component, ViewChild } from "@angular/core";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { ChangeDetectionStrategy } from "@angular/core";

import { ToyComponent } from "./toy.component";
import { Toy } from "src/app/models/toy";

@Component({
  selector: "app-parent",
  template: `
    <app-toy [toy]="toy" (handleRequest)="toySelect($event)"></app-toy>
  `,
})
class ParentComponent {
  @ViewChild(ToyComponent, { static: true }) toyComponent: ToyComponent;
  public toy: Toy;
  public toySelect = (toy: Toy) => toy;
}

describe("ToyComponent", () => {
  let comp: ParentComponent;
  let fixture: ComponentFixture<ParentComponent>;
  let child: ToyComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ParentComponent, ToyComponent],
    }).overrideComponent(ToyComponent, {
      set: { changeDetection: ChangeDetectionStrategy.Default },
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentComponent);
    comp = fixture.componentInstance;
    child = comp.toyComponent;
    comp.toy = {
      title: "toto",
      icon: "ballon",
      selected: true,
    } as Toy;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("should set a toy", () => {
    const el = fixture.debugElement.query(By.css(".toy"));
    const ii = fixture.debugElement.query(By.css("i"));
    fixture.detectChanges();

    expect(el.nativeElement.className).toBe("toy selected");
    expect(ii.nativeElement.className).toBe("mdi mdi-ballon");
  });

  it("should click and select a toy", () => {
    const toy: Toy = { title: "", price: 0, icon: "" };
    spyOn(comp, "toySelect");
    fixture.detectChanges();

    child.select(toy);

    expect(comp.toySelect).toHaveBeenCalledWith(toy);
  });
});
