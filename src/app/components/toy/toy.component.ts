/**
 * ToyComponent
 * selector: 'app-toy'
 *
 * - select : sélection de l'élément
 */
import {
  Component,
  ChangeDetectionStrategy,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { Toy } from "../../models/toy";

@Component({
  selector: "app-toy",
  templateUrl: "./toy.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToyComponent {
  @Input() toy: Toy;
  @Output() handleRequest = new EventEmitter<Toy>();

  select = (toy: Toy): void => {
    this.handleRequest.emit(toy);
  };
}
