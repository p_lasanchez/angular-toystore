/*
 * Testing HeaderContainer
 */

import { TestBed, ComponentFixture } from "@angular/core/testing";
import { DebugElement, NO_ERRORS_SCHEMA } from "@angular/core";
import { Router } from "@angular/router";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";

import { HeaderComponent } from "./header.component";
import { AuthStore } from "../../store/auth/auth.store";
import { ToyStore } from "../../store/toy/toy.store";

describe("HeaderContainer", () => {
  let fixture: ComponentFixture<HeaderComponent>;
  let comp: HeaderComponent;

  const spyAuthStore = jasmine.createSpyObj("spyAuthStore", [
    "setOpen",
    "getConnected",
  ]) as jasmine.SpyObj<AuthStore>;
  const spyToyStore = jasmine.createSpyObj("spyToyStore", [
    "getToyListCount",
  ]) as jasmine.SpyObj<ToyStore>;
  const spyRouter = jasmine.createSpyObj("spyRouter", [
    "navigateByUrl",
  ]) as jasmine.SpyObj<Router>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [
        { provide: AuthStore, useValue: spyAuthStore },
        { provide: ToyStore, useValue: spyToyStore },
        { provide: Router, useValue: spyRouter },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("should get counter", () => {
    spyAuthStore.getConnected.and.returnValue(of(false));
    spyToyStore.getToyListCount.and.returnValue(of(2));
    fixture.detectChanges();

    const target: DebugElement = fixture.debugElement.query(By.css("span"));
    expect(target.nativeElement.textContent).toBe("2");
  });

  it("should open auth", () => {
    spyAuthStore.getConnected.and.returnValue(of(false));
    fixture.detectChanges();
    comp.auth();
    expect(spyAuthStore.setOpen).toHaveBeenCalled();
  });

  it("should navigate if connected", () => {
    spyAuthStore.getConnected.and.returnValue(of(true));
    fixture.detectChanges();
    comp.auth();
    expect(spyRouter.navigateByUrl).toHaveBeenCalledWith("/basket");
  });
});
