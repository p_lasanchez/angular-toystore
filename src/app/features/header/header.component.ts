/**
 * HeaderComponent
 * selector: 'app-header'
 *
 * - auth
 *    . si connecté router vers basket
 *    . si non connecté ouvrir auth popup
 */
import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Subscription } from "rxjs";

import { AuthStore } from "src/app/store/auth/auth.store";
import { ToyStore } from "src/app/store/toy/toy.store";

@Component({
  selector: "app-header",
  templateUrl: "./header.html",
})
export class HeaderComponent implements OnInit, OnDestroy {
  counter$: Observable<number>;
  isConnected$: Observable<boolean>;
  private sub: Subscription;
  private isConnected: boolean;

  constructor(
    private router: Router,
    private authStore: AuthStore,
    private toyStore: ToyStore
  ) {}

  ngOnInit(): void {
    this.counter$ = this.toyStore.getToyListCount();
    this.isConnected$ = this.authStore.getConnected();
    this.sub = this.isConnected$.subscribe((isConnected) => {
      this.isConnected = isConnected;
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  public auth = (): void => {
    if (this.isConnected) {
      this.router.navigateByUrl("/basket");
    } else {
      this.authStore.setOpen();
    }
  };
}
