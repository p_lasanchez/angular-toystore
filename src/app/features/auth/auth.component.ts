/**
 * AuthComponent
 * selector: 'app-auth'
 *
 * - close : ferme popup
 * - connect : connection au store
 */
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { User } from "src/app/models/user";
import { AuthStore } from "../../store/auth/auth.store";

@Component({
  selector: "app-auth",
  templateUrl: "./auth.html",
})
export class AuthComponent implements OnInit {
  isOpened$: Observable<boolean>;
  user: User;

  constructor(private authStore: AuthStore) {}

  ngOnInit(): void {
    this.isOpened$ = this.authStore.getOpened();
    this.user = {
      login: null,
      pass: null,
    };
  }

  public close = (): void => {
    this.authStore.setClose();
  };

  public connect = (): void => {
    this.authStore.setConnected(this.user);
  };
}
