/*
 * Testing AuthComponent
 */

import { TestBed, ComponentFixture } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AuthComponent } from "./auth.component";
import { AuthStore } from "../../store/auth/auth.store";
import { User } from "src/app/models/user";
import { of } from "rxjs";

describe("AuthComponent", () => {
  let fixture: ComponentFixture<AuthComponent>;
  let comp: AuthComponent;

  const spyAuthStore = jasmine.createSpyObj("spyAuth", [
    "setClose",
    "setConnected",
    "getOpened",
  ]) as jasmine.SpyObj<AuthStore>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [AuthComponent],
      providers: [{ provide: AuthStore, useValue: spyAuthStore }],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("Should init", (done) => {
    spyAuthStore.getOpened.and.returnValue(of(true));
    fixture.detectChanges();
    expect(comp.user).toEqual({ login: null, pass: null });
    comp.isOpened$.subscribe((res) => {
      expect(res).toBe(true);
      done();
    });
  });

  it("should close", () => {
    comp.close();
    fixture.detectChanges();
    expect(spyAuthStore.setClose).toHaveBeenCalled();
  });

  it("should connect", () => {
    const theUser: User = { login: "toto", pass: "123" };
    comp.user = theUser;
    comp.connect();
    fixture.detectChanges();
    expect(spyAuthStore.setConnected).toHaveBeenCalledWith(theUser);
  });
});
