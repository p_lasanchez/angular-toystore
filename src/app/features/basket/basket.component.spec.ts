/*
 * Testing BasketComponent
 */

import { TestBed, ComponentFixture } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";

import { BasketComponent } from "./basket.component";
import { ToyStore } from "../../store/toy/toy.store";
import { of } from "rxjs";
import { Toy } from "src/app/models/toy";

describe("BasketComponent", () => {
  let fixture: ComponentFixture<BasketComponent>;
  let comp: BasketComponent;

  const mockToyList: Toy[] = [
    { title: "", icon: "", price: 100, selected: true },
  ];
  const spyToyStore = jasmine.createSpyObj("spyToyStore", [
    "setSelectedToy",
    "getSelectedToys",
    "getTotalPrice",
  ]) as jasmine.SpyObj<ToyStore>;

  spyToyStore.getTotalPrice.and.returnValue(of(100));
  spyToyStore.getSelectedToys.and.returnValue(of(mockToyList));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasketComponent],
      providers: [
        {
          provide: ToyStore,
          useValue: spyToyStore,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketComponent);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("should get toys", (done) => {
    fixture.detectChanges();

    comp.toyList$.subscribe((res) => {
      expect(res.length).toBe(1);
      done();
    });
  });

  it("should delete a toy", () => {
    fixture.detectChanges();
    comp.delete(mockToyList[0]);
    expect(spyToyStore.setSelectedToy).toHaveBeenCalledWith(mockToyList[0]);
  });

  it("should compute the price", (done) => {
    fixture.detectChanges();

    comp.price$.subscribe((res) => {
      expect(res).toBe(100);
      done();
    });
  });
});
