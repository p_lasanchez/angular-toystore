/**
 * BasketComponent
 * selector: 'app-basket'
 *
 * - delete : supprimer élément
 */
import { Component, OnInit } from "@angular/core";
import { Toy } from "../../models/toy";
import { ToyStore } from "src/app/store/toy/toy.store";
import { Observable } from "rxjs";

@Component({
  selector: "app-basket",
  templateUrl: "./basket.html",
})
export class BasketComponent implements OnInit {
  toyList$: Observable<Toy[]>;
  price$: Observable<number>;

  constructor(private toyStore: ToyStore) {}

  ngOnInit(): void {
    this.toyList$ = this.toyStore.getSelectedToys();
    this.price$ = this.toyStore.getTotalPrice();
  }

  public delete = (toy: Toy): void => {
    this.toyStore.setSelectedToy(toy);
  };
}
