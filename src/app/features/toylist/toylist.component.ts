/**
 * ToysComponent
 * selector: 'app-toys'
 *
 * - onSelect : select element
 */
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Toy } from "../../models/toy";
import { ToyStore } from "src/app/store/toy/toy.store";

@Component({
  selector: "app-toys",
  templateUrl: "./toylist.html",
})
export class ToyListComponent implements OnInit {
  toyList$: Observable<Toy[]>;

  constructor(private toyStore: ToyStore) {}

  ngOnInit(): void {
    this.toyList$ = this.toyStore.getToyList();
  }

  onSelect = (toy: Toy): void => {
    this.toyStore.setSelectedToy(toy);
  };
}
