/*
 * Testing ToyListComponent
 */

import { TestBed, ComponentFixture } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";

import { ToyListComponent } from "./toylist.component";
import { Toy } from "../../models/toy";
import { ToyStore } from "src/app/store/toy/toy.store";

describe("ToyListComponent", () => {
  let fixture: ComponentFixture<ToyListComponent>;
  let comp: ToyListComponent;

  const spyToyStore = jasmine.createSpyObj("spyToyStore", [
    "setSelectedToy",
    "getToyList",
  ]) as jasmine.SpyObj<ToyStore>;

  const mockToys: Toy[] = [
    { selected: true, icon: null, title: null, price: 0 },
    { selected: false, icon: null, title: null, price: 0 },
    { selected: true, icon: null, title: null, price: 0 },
  ];
  spyToyStore.getToyList.and.returnValue(of(mockToys));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ToyListComponent],
      providers: [{ provide: ToyStore, useValue: spyToyStore }],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToyListComponent);
    comp = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("should get toys", (done) => {
    fixture.detectChanges();

    const toys = fixture.debugElement.queryAll(By.css("app-toy"));
    expect(toys.length).toBe(3);
    expect(toys[0].nativeElement.toy).toEqual(mockToys[0]);

    comp.toyList$.subscribe((res) => {
      expect(res).toEqual(mockToys);
      done();
    });
  });

  it("should select a toy", () => {
    const toy = { title: "", icon: "", price: 0 };
    comp.onSelect(toy);
    fixture.detectChanges();

    expect(spyToyStore.setSelectedToy).toHaveBeenCalledWith(toy);
  });
});
