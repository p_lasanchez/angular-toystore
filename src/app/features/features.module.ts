/**
 * FeaturesModule
 * - ComponentsModule
 * - CommonModule
 * - FormsModule
 * - RouterModule
 *
 * - AuthComponent
 * - BasketComponent
 * - HeaderComponent
 * - ToyListComponent
 */
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ComponentsModule } from "../components/components.module";

import { AuthComponent } from "./auth/auth.component";
import { BasketComponent } from "./basket/basket.component";
import { HeaderComponent } from "./header/header.component";
import { ToyListComponent } from "./toylist/toylist.component";

const features = [
  AuthComponent,
  BasketComponent,
  HeaderComponent,
  ToyListComponent,
];

@NgModule({
  imports: [ComponentsModule, CommonModule, FormsModule, RouterModule],
  declarations: [...features],
  exports: [...features],
})
export class FeaturesModule {}
